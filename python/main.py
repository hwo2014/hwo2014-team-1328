import json
import socket
import sys
import logging
from Coche import Coche


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'


class CarnageBot(object):

    def __init__(self, socket, name, key):
        logging.basicConfig(level=logging.ERROR)
        logging.StreamHandler().setLevel(logging.ERROR)
        self.socket = socket
        self.name = name
        self.key = key

        self.velocidad = 0.0
        self.angulo = 0.0
        self.aceleracion = 0.0
        self.coches = {}

    def p(self, msg, type=bcolors.ENDC):
         print(type + msg + bcolors.ENDC)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.aceleracion = throttle
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        self.p("Joined")
        logging.info("Joined:" + str(data))
        self.ping()

    def on_game_init(self, data):
        self.p("Game Init", bcolors.HEADER)
        logging.info("Game init:" + str(data))
        #Inicializamos los coches de la pista
        for c in data['race']['cars']:
            coche = Coche(c['id']['name'], c['id']['color'], c['dimensions']['length'], c['dimensions']['width'], c['dimensions']['guideFlagPosition'])
            self.coches[coche.name + coche.color] = coche
        self.ping()

    def on_game_start(self, data):
        self.p("Race started")
        logging.info("Race started:" + str(data))
        self.ping()

    #########################################################
    #                                                       #
    #########################################################
    def on_car_positions(self, data):
        logging.info("Car Position :" + str(data))
        #Tratamos los datos de cada uno de los coches
        for coche in data:
            c = self.coches[coche['id']['name']+coche['id']['color']]
            #Actualizamos la info de cada coche
            c.actualizarDatos(coche)

            #Al recibir los datos de nuestro coche, calculamos la aceleracion
            if (c.name==self.name):
                if (abs(c.delta_angulo>0.4)):
                    self.aceleracion = self.aceleracion - 0.2
                    if (self.aceleracion<=0.0):
                        self.aceleracion=0.15
                    self.throttle(self.aceleracion)
                else:
                    self.throttle(0.8)
        #Calcular mi posicion, velocidad, aceleracion y nivel de agarre
            #Si estamos en pieza sin curva aceleramos
            #Deceleramos en funcion de nuestro angulo, curva de pieza y velocidad
        #Aprender de los errores de los demas
        #Aprender circuito

        #self.throttle(0.65)

    def on_crash(self, data):
        self.p("Someone crashed", bcolors.FAIL)
        logging.info("Someone crashed" + str(data))
        self.ping()

    def on_game_end(self, data):
        self.p("Race ended")
        logging.info("Race ended" + str(data))
        self.ping()

    def on_error(self, data):
        self.p("Error: {0}".format(data), bcolors.FAIL)
        logging.error("Error: {0}".format(data) + str(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                self.p("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = CarnageBot(s, name, key)
        bot.run()
