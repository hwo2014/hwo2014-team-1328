__author__ = 'Marcos'

#Objeto que representa los datos y metodos de cada coche
class Coche(object):

    def __init__(self, name, color, length, width, guideFlagPosition):
        self.name = name
        self.color = color

        self.largo = length
        self.ancho = width
        self.guia = guideFlagPosition

        self.angulo = 0.0
        self.pieza = 0
        self.pieza_posicion = 0.0

        self.linea_ini = 0
        self.cambio_linea = False

        self.velocidad = 0.0
        self.aceleracion = 0.0

        #Indica la variacion del angulo del coche
        self.delta_angulo = 0.0

    # Le pasamos el bloque de datos del coche devuelto por CARPOSITIONS y lo parsea guardando y generando la informacion
    # que nos sera util a posteriori
    def actualizarDatos(self, data):

        if (data['piecePosition']['pieceIndex']==self.pieza):
            self.velocidad = data['piecePosition']['inPieceDistance']-self.pieza_posicion

        self.delta_angulo = abs(data['angle'] - self.angulo)

        #Actualizamos los datos del coche
        self.angulo = data['angle']
        self.pieza = data['piecePosition']['pieceIndex']
        self.pieza_posicion = data['piecePosition']['inPieceDistance']
        self.linea_ini = data['piecePosition']['lane']['startLaneIndex']
        self.cambio_linea = (data['piecePosition']['lane']['startLaneIndex'] != data['piecePosition']['lane']['endLaneIndex'])

        print("\033[93m Pieza: {0}, Recorrido: {1}, Angulo: {2}, DeltaAngulo: {4}, Velocidad: {3}".format(str(self.pieza), str(self.pieza_posicion), str(self.angulo), str(self.velocidad) , str(self.delta_angulo) ))